# MITRE ATT&CK Kill Chain

## Reconnaissance [TA0043]

### Active Scanning [T1595]

Scan of the target system including operating system detection.

```
nmap -sT -O -A 192.168.56.103

nmap -sU -O -A 192.168.56.103
```

dig -x 192.168.56.103

## Resource Development [TA0042]

## Initial Access [TA0001]

## Execution [TA0002]

## Persistence [TA0003]

## Privilege Escalation [TA0004]

## Defense Evasion [TA0005]

## Credential Access [TA0006]

## Discovery [TA0007]

## Lateral Movement [TA0008]

## Collection [TA0009]

## Command and Control [TA0011]

## Exfiltration [TA0010]

## Impact [TA0040]

